package Main;

import Puzzle.BoardUtils;
import Puzzle.GUI;

public class Main {
  public static void main(String[] args) {
    GUI.start();
  }

  public static void startGameButton() {
    int[] board = BoardUtils.generate();

    // Sending cloned array to isSolvable, so it doesn't change the original array.
    while (!isSolvable(board.clone())) {
      board = BoardUtils.generate();
    }

    GUI.display(board);
  }

  public static void solveButton(int[] board) {
    BoardUtils.solve(board);
  }

  // TODO: check if board is solvable
  public static boolean isSolvable(int[] board) {
    return true;
  }

  // TODO: Calculate board difficulty
  public static int calculateDifficulty(int[] board) {
    return 1;
  }

  // TODO: convert board array to hash
  public static String hash(int[] board) {
    return "";
  }

  // TODO: get the index of hash in array, or the specified index if not present in the array
  public static int find(String hash, String[] existingHashes) {
    return 0;
  }
}
