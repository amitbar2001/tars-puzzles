package Puzzle;

import java.util.*;
import java.util.stream.Collectors;
import Main.Main;

public class AStar {
  private static final int COST_CEILING = 70;
  private static final int INITIAL_CAPACITY = 10000;

  protected static Node solve(State state) {
    return Run(
        new PriorityQueue<Node>() {
          {
            add(new Node(state.copy()));
          }
        },
        new ArrayList<String>(INITIAL_CAPACITY) {
          {
            add(tarsAdapter(state.copy()));
          }
        });
  }

  protected static Node Run(PriorityQueue<Node> open, ArrayList<String> closed) {
    long start = System.currentTimeMillis();
    updateUI();

    while (!open.isEmpty()) {
      Node current = open.remove();

      updateUI(current.state());

      if (current.isSolved()) {
        System.out.println((System.currentTimeMillis() - start) + " milliseconds elapsed");
        return current;
      }

      open.addAll(filterSuccessors(current.successors(), closed));
    }

    return null;
  }

  private static List<Node> filterSuccessors(List<Node> successors, ArrayList<String> closed) {
    return successors.stream()
        .filter(successor -> shouldBeAdded(successor, closed))
        .collect(Collectors.toList());
  }

  private static boolean shouldBeAdded(Node successor, ArrayList<String> closed) {
    boolean shouldBeAdded = true;

    if (successor.cost() < COST_CEILING) {
      String successorHash = tarsAdapter(successor.state());
      String[] hashes = new String[closed.size()];
      closed.toArray(hashes);

      int pos = Main.find(successorHash, hashes);

      if (pos < 0) {
        closed.add(-pos - 1, successorHash);
      } else {
        shouldBeAdded = false;
      }
    } else {
      shouldBeAdded = false;
    }

    return shouldBeAdded;
  }

  private static String tarsAdapter(State state) {
    int[] board = BoardUtils.flatten(((Board) state).matrix());

    return Main.hash(board);
  }

  private static void updateUI(State current) {
    ConsoleUI.update(current);
  }

  private static void updateUI() {
    ConsoleUI.start();
  }
}
