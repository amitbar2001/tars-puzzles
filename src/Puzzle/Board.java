package Puzzle;

import Main.Main;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Board extends State {
  public static final int LINE_SIZE = 4;
  public static final int BOARD_SIZE = LINE_SIZE * LINE_SIZE;
  public static final int BLANK_MARKER = BOARD_SIZE;
  public static final Board SOLVED = new Board();

  private final int[][] matrix;
  private Dot blankPos;

  public Board(int[][] matrix, Dot blankPos) {
    this.matrix = matrix;
    this.blankPos = blankPos;
  }

  private Board(Board original) {
    // Copy Constructor

    this.matrix = Arrays.stream(original.matrix()).map(int[]::clone).toArray(int[][]::new);
    this.blankPos = original.blankPos().copy();
  }

  public Board(int[][] matrix) {
    this(matrix, Board.blankPosOf(matrix));
  }

  public Board() {
    this(Board.baseState());
  }

  public int[][] matrix() {
    return this.matrix;
  }

  public Dot blankPos() {
    return this.blankPos;
  }

  public Board copy() {
    return new Board(this);
  }

  public void makeMove(Move move) {
    Dot newBlank = move.on(this.blankPos());
    this.matrix()[this.blankPos().y()][this.blankPos().x()] =
        this.matrix()[newBlank.y()][newBlank.x()];
    this.matrix()[newBlank.y()][newBlank.x()] = Board.BLANK_MARKER;
    this.changeBlankPos(newBlank);
  }

  private void changeBlankPos(Dot blankPos) {
    this.blankPos = blankPos;
  }

  private static int[][] baseState() {
    int[][] state = new int[Board.LINE_SIZE][Board.LINE_SIZE];

    for (int i = 0; i < Board.LINE_SIZE; i++) {
      for (int j = 0; j < Board.LINE_SIZE; j++) {
        state[i][j] = i * Board.LINE_SIZE + j + 1;
      }
    }

    return state;
  }

  private static Dot blankPosOf(int[][] state) {
    Dot blankPos = null;

    for (int i = 0; i < state.length; i++) {
      for (int j = 0; j < state[0].length; j++) {
        if (state[i][j] == Board.BOARD_SIZE) {
          blankPos = new Dot(j, i);
        }
      }
    }

    return blankPos;
  }

  @Override
  public boolean isSolved() {
    return super.isSolved() && this.matrix()[LINE_SIZE - 1][LINE_SIZE - 1] == BLANK_MARKER;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Board board = (Board) o;
    return Arrays.deepEquals(this.matrix(), board.matrix())
        && this.blankPos().equals(board.blankPos());
  }

  @Override
  public int hashCode() {
    int result = this.blankPos().hashCode();
    result = 31 * result + Arrays.deepHashCode(this.matrix());
    return result;
  }

  @Override
  public List<State> successors() {

    return Move.getPossible(this.blankPos()).stream()
        .map(
            possibleMove -> {
              Board clone = this.copy();
              clone.makeMove(possibleMove);

              return clone;
            })
        .collect(Collectors.toList());
  }

  @Override
  public int calcHeuristic() {
    return Main.calculateDifficulty(BoardUtils.flatten(this.matrix()));
  }
}
