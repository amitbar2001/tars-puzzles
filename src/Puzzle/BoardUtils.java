package Puzzle;

public class BoardUtils {
  public static final int BOARD_SIZE = 16;
  public static final int LINE_SIZE = (int) Math.sqrt(BOARD_SIZE);

  public static int[] generate() {
    Board board = Generator.generate();
    return flatten(board.matrix());
  }

  public static void solve(int[] array) {
    Board board = new Board(toMatrix(array));
    GUIAdapter.solve(board);
  }

  protected static int[] flatten(int[][] matrix) {
    int[] array = new int[matrix.length * matrix[0].length];

    for (int i = 0; i < matrix.length; i++) {
      System.arraycopy(matrix[i], 0, array, i * matrix.length, matrix[0].length);
    }

    return array;
  }

  protected static int[][] toMatrix(int[] board) {
    int[][] matrix = new int[LINE_SIZE][LINE_SIZE];

    for (int i = 0; i < LINE_SIZE; i++) {
      System.arraycopy(board, i * LINE_SIZE, matrix[i], 0, LINE_SIZE);
    }

    return matrix;
  }
}
