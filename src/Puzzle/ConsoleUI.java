package Puzzle;

import java.util.Timer;

public class ConsoleUI {
  private static int lowest = 100;
  private static int min = 0;
  private static int dotCount = 0;
  private static int maxDotCount = 3;
  private static boolean run = false;
  private static boolean failed = false;
  private static Timer timer = new java.util.Timer();

  public static void start() {
    System.out.println("Starting to solve board");
    lowest = 100;
    run = true;
    failed = false;
    print();
    update();
  }

  public static void update(State state) {
    int heuristic = state.heuristic();

    if (heuristic == min) {
      run = false;
      lowest = heuristic;
      print(lowest);
      print();
    } else if (heuristic < lowest) {
      lowest = heuristic;
      print(lowest);
    }
  }

  private static void print() {
    if (run) {
      System.out.print("\r");
      System.out.print("Searching");
      dotCount = ((dotCount + 1) % maxDotCount) + 1;
      for (int i = 0; i < dotCount; i++) System.out.print(".");
    } else {
      if (!failed) {
        System.out.println("Board solved!");
      } else {
        System.out.println("Failed to solve board!");
      }
    }
  }

  private static void print(int lowest) {
    System.out.println("\rLowest difficulty => " + lowest + " ");
  }

  protected static void failed() {
    run = false;
    failed = true;
    print();
  }


  private static void update() {
    try {
      timer.schedule(
              new java.util.TimerTask() {
                @Override
                public void run() {
                  if (run) {
                    print();
                    update();
                  } else {
                    timer.cancel();
                    timer = new java.util.Timer();
                  }
                }
              },
              50);
    } catch (IllegalStateException e) {
      // finished faster than previous solve task queue
    }
  }
}