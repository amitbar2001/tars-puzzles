package Puzzle;

import java.util.Objects;

public class Dot {
  private final int x;
  private final int y;

  public Dot(int x, int y) {
    this.x = x;
    this.y = y;
  }

  private Dot(Dot original) {
    this.x = original.x();
    this.y = original.y();
  }

  public int x() {
    return this.x;
  }

  public int y() {
    return y;
  }

  public Dot copy() {
    return new Dot(this);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Dot dot = (Dot) o;
    return this.x() == dot.x() && this.y() == dot.y();
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.x(), this.y());
  }
}
