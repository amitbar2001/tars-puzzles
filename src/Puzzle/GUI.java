package Puzzle;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Stack;

import javax.swing.*;

import static Main.Main.startGameButton;
import static Puzzle.BoardUtils.toMatrix;

// We are going to create a Game of 15 Puzzle with Java 8 and Swing
// If you have some questions, feel free to ue comments ;)
public class GUI extends JPanel { // our grid will be drawn in a dedicated Panel

  // Size of our Game of Fifteen instance
  private final int size;
  // Number of tiles
  private final int nbTiles;
  // Grid UI Dimension
  private final int dimension;
  // Foreground Color
  private static final Color FOREGROUND_COLOR = new Color(239, 83, 80); // we use arbitrary color
  // Storing the tiles in a 1D Array of integers
  private final int[] tiles;
  // a stack that contains all the moves in the game
  private static Stack<int[][]> gamePlay = new Stack<>();
  // game difficulty
  // Size of tile on UI
  private final int tileSize;
  // Position of the blank tile
  private int blankPos;
  // Margin for the grid on the frame
  private final int margin;
  // Grid UI Size
  private final int gridSize;
  // the game starts after the user clicks twice
  private boolean isTwoClicks;
  private static int[] board;
  private boolean firstGame = true;
  private static final int plateSize = 106;
  private static final int colSize = 121;
  private static final int rowSize = 121;
  private static final int rowPrefix = 100;
  private static final int colPrefix = 215;
  private static final int yLocation = 120;
  private static boolean isSolving = false;
  private static boolean eilay = false;
  private static boolean linch = false;
  private boolean gameOver;
  final Color[] colorTable = {
          new Color(0xFFE4C3), new Color(0xffdac3), new Color(0xe7b08e), new Color(0x701710)
  };
  private final int[] colorToNumber;

  public GUI(int size, int dim, int mar) {
    this.size = size;
    dimension = dim;
    margin = mar;
    gameOver = false;
    isTwoClicks = false;

    // init tiles
    nbTiles = size * size - 1;
    tiles = new int[size * size];

    // init color to number array
    this.colorToNumber = new int[size * size];

    // calculate grid size and tile size
    gridSize = (dim - 2 * margin);
    tileSize = gridSize / size;

    setPreferredSize(new Dimension(dimension, dimension - margin));
    setBackground(new Color(0xFAF8EF));
    setForeground(FOREGROUND_COLOR);
    setFont(new Font("arial", Font.BOLD, 40));

    addMouseListener(
            new MouseAdapter() {
              @Override
              public void mousePressed(MouseEvent e) {
                mouseListenerActions(e);
              }
            });
  }

  private void mouseListenerActions(MouseEvent e) {
    // get position of the click
    int ex = e.getX() - margin;
    int ey = e.getY() - margin;
    if (gameOver) {
      isTwoClicks = true;
      firstGame = false;
      startGameButton();
      newGame();
    } else if (firstGame && inStartButtonLimints(ex, ey)) {
      isTwoClicks = true;
      firstGame = false;
      startGameButton();
      newGame();
    } else if (firstGame) {
    } else {
      // happens if the player hits the auto solve button
      if (inButtonLimits(ex, ey) && !isSolving) {
        solveButton();
      }

      if (ex < 0 || ex > gridSize || ey + yLocation < 0 || ey + yLocation > gridSize) return;

      // get position in the grid
      int c1 = ex / tileSize;
      int r1 = (ey + yLocation) / tileSize;

      // get position of the blank cell
      int c2 = blankPos % size;
      int r2 = blankPos / size;

      // we convert in the 1D coord
      int clickPos = r1 * size + c1;
      int dir = 0;

      // we search direction for multiple tile moves at once
      if (c1 == c2 && Math.abs(r1 - r2) > 0) dir = (r1 - r2) > 0 ? size : -size;
      else if (r1 == r2 && Math.abs(c1 - c2) > 0) dir = (c1 - c2) > 0 ? 1 : -1;

      if (dir != 0) {
        // we move tiles in the direction
        do {
          int newBlankPos = blankPos + dir;
          tiles[blankPos] = tiles[newBlankPos];
          colorToNumber[blankPos] = colorToNumber[newBlankPos];
          blankPos = newBlankPos;
        } while (blankPos != clickPos);

        tiles[blankPos] = Board.BLANK_MARKER;
      }

      gameOver = isSolved();
    }

    repaint();
  }

  private void solveButton() {
    isSolving = true;
    GUIAdapter.solveButton(tiles);

    // repeats in every move until it solved
    // TODO: set delay in opposite relation to gamePlay.size
    //  more moves - faster | less moves - slower
    javax.swing.Timer timer =
            new Timer(
                    100,
                    evt -> {
                      if (gamePlay.size() == 1 && !linch) {
                        eilay = true;
                        linch = true;
                        java.util.Timer delayTimer = new java.util.Timer();
                        delayTimer.schedule(
                                new java.util.TimerTask() {
                                  @Override
                                  public void run() {
                                    eilay = false;
                                  }
                                },
                                500);
                      } else if (gamePlay.size() != 0 && !eilay) {
                        setTiles();
                        repaint();
                        gamePlay.pop();
                      } else if (gamePlay.size() == 0) {
                        ((Timer) evt.getSource()).stop();
                        gameOver = true;
                        isSolving = false;
                        linch = false;
                        repaint();
                      }
                    });
    timer.start();
  }

  public static void display(int[] board) {
    gamePlay.push(toMatrix(board));
  }

  // is the user pressing the button in its limits?
  private boolean inButtonLimits(int ex, int ey) {
    return ey >= 50 && ey <= 200 && ex >= -180 && ex <= -40;
  }

  private boolean inStartButtonLimints(int ex, int ey) {
    return ey > -1 && ey < 300 && ex < 400 && ex > 100;
  }

  private void newGame() {
    setTiles();
    tiles[15] = Board.BLANK_MARKER;

    // we set blank cell at the last
    blankPos = tiles.length - 1;
    gameOver = false;
  }

  private void setTiles() {
    int count = 0;
    int[][] boardToSolve = gamePlay.peek();

    for (int index = 0; index < 4; index++) {
      for (int j = 0; j < 4; j++) {
        tiles[count] = boardToSolve[index][j];

        // giving a certain color to every 4 numbers, so the soled board will always look the same
        if (tiles[count] <= 4) {
          colorToNumber[count] = 0;
        } else if (tiles[count] <= 8) {
          colorToNumber[count] = 1;
        } else if (tiles[count] <= 12) {
          colorToNumber[count] = 2;
        } else {
          colorToNumber[count] = 3;
        }
        count++;
      }
    }
  }

  private boolean isSolved() {
    // if blank tile is not in the solved position ==> not solved
    if (tiles[tiles.length - 1] != Board.BLANK_MARKER) return false;

    for (int i = nbTiles - 1; i >= 0; i--) {
      if (tiles[i] != i + 1) return false;
    }

    return true;
  }

  // TODO: animate tile move so it slides from its place to the empty spot

  private void drawGrid(Graphics2D g) {
    if (isTwoClicks && !firstGame) {
      g.setColor(new Color(0x5E0404));
      String t = "פאזל 15";
      g.drawString(t, (getWidth() - g.getFontMetrics().stringWidth(t)) / 2, 60);

      // draws every tile
      for (int i = 0; i < tiles.length; i++) {
        // we convert 1D coords to 2D coords given the size of the 2D Array
        int r = i / size;
        int c = i % size;
        // we convert in coords on the UI
        int x = margin + c * tileSize;
        int y = margin - yLocation + r * tileSize;
        // check special case for blank tile
        if (tiles[i] == Board.BLANK_MARKER) {
          g.setColor(new Color(0xBBADA0));
          g.fillRoundRect(x, y, tileSize, tileSize, 0, 0);
          g.drawRoundRect(x, y, tileSize, tileSize, 0, 0);
          g.setColor(new Color(0xCDC1B4));
          g.fillRoundRect(
                  colPrefix + c * colSize, rowPrefix + r * rowSize, plateSize, plateSize, 15, 15);

          if (gameOver) {
            g.setColor(new Color(0x10704B));
            drawCenteredString(g, "לחץ כדי להתחיל משחק חדש", 390, 550);
          }

          continue;
        }

        // for other tiles
        g.setColor(new Color(0xBBADA0));
        g.fillRoundRect(x, y, tileSize, tileSize, 0, 0);
        g.drawRoundRect(x, y, tileSize, tileSize, 0, 0);
        g.setColor(colorTable[colorToNumber[i]]);
        g.fillRoundRect(
                colPrefix + c * colSize, rowPrefix + r * rowSize, plateSize, plateSize, 15, 15);
        if (colorToNumber[i] < 3) {
          g.setColor(new Color(0x701710));
        } else {
          g.setColor(new Color(0xFFDAC3));
        }

        FontMetrics fm = g.getFontMetrics();
        int asc = fm.getAscent();
        int dec = fm.getDescent();

        int xstr =
                colPrefix + c * colSize + (plateSize - fm.stringWidth(String.valueOf(tiles[i]))) / 2;
        int ystr = rowPrefix + r * rowSize + (asc + (plateSize - (asc + dec)) / 2);
        g.drawString(String.valueOf(tiles[i]), xstr, ystr);
      }
    }
  }

  private void drawStartMessage(Graphics2D g) {
    if (firstGame) {
      String str = "שחק!";
      g.setColor(new Color(0x150505));
      g.drawOval(300, 200, 300, 300);
      g.setColor(new Color(0x5E0404));
      g.fillOval(305, 205, 290, 290);
      g.setFont(getFont().deriveFont(Font.BOLD, 50));
      g.setColor(new Color(0xFFEDED));
      g.drawString(str, 390, getHeight() - 330);
    }
  }

  private void drawCenteredString(Graphics2D g, String s, int x, int y) {
    // center string s for the given tile (x,y)
    FontMetrics fm = g.getFontMetrics();
    int asc = fm.getAscent();
    int desc = fm.getDescent();
    g.drawString(
            s, x + (tileSize - fm.stringWidth(s)) / 2, y + (asc + (tileSize - (asc + desc)) / 2));
  }

  @Override
  protected void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2D = (Graphics2D) g;
    g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    drawGrid(g2D);
    drawStartMessage(g2D);
    drawSolveButton(g2D);
  }

  private void drawSolveButton(Graphics2D g) {
    if (isTwoClicks && !firstGame) {
      String str = "תפתור לי!";
      g.setColor(new Color(0x325D5E));
      g.fillOval(20, 250, 140, 140);
      g.setFont(getFont().deriveFont(Font.BOLD, 25));
      g.setColor(new Color(0xFFEDED));
      g.drawString(str, 37, 330);
    }
  }

  public static void start() {
    SwingUtilities.invokeLater(
            () -> {
              JFrame frame = new JFrame();
              frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
              frame.setTitle("Game of Fifteen");
              frame.setResizable(false);
              frame.add(new GUI(4, 900, 200), BorderLayout.CENTER);
              frame.pack();
              // center on the screen
              frame.setLocationRelativeTo(null);
              frame.setVisible(true);
            });
  }

  protected static void solve(Stack<int[][]> board) {
    gamePlay = board;
  }
}
