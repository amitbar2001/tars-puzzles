package Puzzle;

import java.util.Stack;

public class GUIAdapter {
  public static void solve(State state) {
    Stack<int[][]> solution = new Stack<>();
    Node current = AStar.solve(state);

    if (current != null) {
      while (current != null) {
        int[][] step = ((Board) current.state()).matrix();
        solution.push(step);
        current = current.parent();
      }

      GUI.solve(solution);
    } else {
      ConsoleUI.failed();
    }
  }

  protected static void solveButton(int[] board) {
    Main.Main.solveButton(board);
  }
}
