package Puzzle;

import java.util.*;
import java.util.stream.Collectors;

public class Generator {
  private static final int MIN_MOVES = 50;
  private static final int RANGE_MOVES = 3;

  public static Board generate() {
    if (Math.random() > 0.7) {
      return generateValid();
    } else {
      return generateInvalid();
    }
  }

  public static Board generateValid() {
    Board board = new Board();
    HashSet<Board> visitedStates =
        new HashSet<Board>() {
          {
            add(board.copy());
          }
        };

    shuffle(board, visitedStates);
    getBlankToCorner(board, visitedStates);

    return board;
  }

  private static Board generateInvalid() {
    final int CORRUPT_INDEX = 1;

    Board valid = generateValid();
    int[][] matrixToCorrupt = valid.matrix();
    int temp = matrixToCorrupt[CORRUPT_INDEX][CORRUPT_INDEX];
    matrixToCorrupt[CORRUPT_INDEX][CORRUPT_INDEX] =
        matrixToCorrupt[CORRUPT_INDEX + 1][CORRUPT_INDEX + 1];
    matrixToCorrupt[CORRUPT_INDEX + 1][CORRUPT_INDEX + 1] = temp;

    return new Board(matrixToCorrupt);
  }

  private static void shuffle(Board board, HashSet<Board> visitedStates) {
    Random rnd = new Random();
    int numbersOfMoves = rnd.nextInt(Generator.RANGE_MOVES) + Generator.MIN_MOVES;
    List<Move> availableMoves = getPossibleSteps(board, visitedStates);

    while (numbersOfMoves > 0 && availableMoves.size() > 0) {
      Move randomMove = availableMoves.get(rnd.nextInt(availableMoves.size()));
      doMove(randomMove, board, visitedStates);
      availableMoves = getPossibleSteps(board, visitedStates);
      numbersOfMoves--;
    }
  }

  private static void getBlankToCorner(Board board, HashSet<Board> visitedStates) {
    while (!board.blankPos().equals(Board.SOLVED.blankPos())) {
      List<Move> availableMoves = getPossibleSteps(board, visitedStates);

      Move move;

      if (availableMoves.size() == 0) {
        move = Move.getPossible(board.blankPos()).get(0);
      } else {
        move = availableMoves.get(0);
      }

      doMove(move, board, visitedStates);
    }
  }

  private static List<Move> getPossibleSteps(Board board, HashSet<Board> visitedStates) {
    return Move.getPossible(board.blankPos()).stream()
        .filter(
            possibleMove -> {
              Board duplicate = board.copy();
              duplicate.makeMove(possibleMove);
              return !visitedStates.contains(duplicate);
            })
        .collect(Collectors.toList());
  }

  private static void doMove(Move move, Board board, HashSet<Board> visitedStates) {
    board.makeMove(move);
    visitedStates.add(board.copy());
  }
}
