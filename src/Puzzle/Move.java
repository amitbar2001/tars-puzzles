package Puzzle;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Move extends Dot {
  private static final List<Move> MOVES =
      new ArrayList<Move>() {
        {
          add(new Move(0, 1));
          add(new Move(1, 0));
          add(new Move(0, -1));
          add(new Move(-1, 0));
        }
      };

  public Move(int x, int y) {
    super(x, y);
  }

  public Dot on(Dot pos) {
    return new Dot(super.x() + pos.x(), super.y() + pos.y());
  }

  public static List<Move> getPossible(Dot blankPos) {
    return MOVES.stream().filter(move -> move.isMoveLegal(blankPos)).collect(Collectors.toList());
  }

  private boolean isMoveLegal(Dot pos) {
    Dot resultPos = this.on(pos);

    return resultPos.x() < Board.LINE_SIZE
        && resultPos.x() > -1
        && resultPos.y() < Board.LINE_SIZE
        && resultPos.y() > -1;
  }
}
