package Puzzle;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Node implements Comparable<Node> {
  private final State state;
  private final Node parent;
  private final int COST;
  private final int PRIORITY_SCORE;

  public Node(State state, Node parent) {
    this.state = state;
    this.parent = parent;
    this.COST = this.calcCost();
    this.PRIORITY_SCORE = this.calcPriorityScore();
  }

  public Node(State state) {
    this(state, null);
  }

  public State state() {
    return this.state;
  }

  public int priorityScore() {
    return this.PRIORITY_SCORE;
  }

  public int cost() {
    return this.COST;
  }

  private int calcCost() {
    return this.parent != null ? this.parent.cost() + 1 : 0;
  }

  private int calcPriorityScore() {
    return this.state().isSolved() ? 0 : this.cost() + this.state().heuristic();
  }

  public boolean isSolved() {
    return this.state().isSolved();
  }

  public List<Node> successors() {
    return this.state().successors().stream()
        .map(state -> new Node(state, this))
        .collect(Collectors.toList());
  }

  public Node parent() {
    return this.parent;
  }

  @Override
  public int compareTo(Node o) {
    return priorityScore() - o.priorityScore();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Node node = (Node) o;
    return this.priorityScore() == node.priorityScore() && this.state().equals(node.state());
  }

  @Override
  public int hashCode() {
    int result = Objects.hash(PRIORITY_SCORE);
    result = 31 * result + this.state().hashCode();
    return result;
  }
}
