package Puzzle;

import java.util.List;

public abstract class State {
  private int heuristic = Integer.MAX_VALUE;

  public State() {}

  public abstract boolean equals(Object o);

  public abstract int hashCode();

  protected abstract int calcHeuristic();

  public int heuristic() {
    if (this.heuristic == Integer.MAX_VALUE) this.heuristic = calcHeuristic();

    return this.heuristic;
  }

  public boolean isSolved() {
    return this.heuristic() == 0;
  }

  public abstract List<State> successors();

  public abstract State copy();
}
